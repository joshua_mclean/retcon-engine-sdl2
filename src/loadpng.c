#include "loadpng.h"
#include "types.h"

int width = -1, height = -1;
png_byte color_type;
png_byte bit_depth;
png_bytep *row_pointers;

bool is_png_open = false;

void
png_getSize( int *w, int *h )
{
        *w = width;
        *h = height;
}

int
png_close()
{
        if ( is_png_open ) {
                fprintf( stderr, "Tried to close png file but none is open." );
                return 1;
        }

        // release memory
        for ( int y = 0; y < height; y++ ) {
                free( row_pointers[y] );
        }
        free( row_pointers );

        // reset values to "error" values
        width = -1;
        height = -1;
        row_pointers = NULL;
        is_png_open = false;

        return 0;
}

int
png_load( const char *const file_name )
{
        if ( is_png_open ) {
                fprintf( stderr, "PNG file already open - can't load another "
                                 "one until it's closed.\n" );
                return 1;
        }

        FILE *fp = fopen( file_name, "rb" );

        png_structp png =
            png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
        if ( !png ) {
                return 1;
        }

        png_infop info = png_create_info_struct( png );
        if ( !info ) {
                return 1;
        }

        if ( setjmp( png_jmpbuf( png ) ) ) {
                abort();
        }

        png_init_io( png, fp );

        png_read_info( png, info );

        width = png_get_image_width( png, info );
        height = png_get_image_height( png, info );
        color_type = png_get_color_type( png, info );
        bit_depth = png_get_bit_depth( png, info );

        // Read any color_type into 8bit depth, RGBA format.
        // See http://www.libpng.org/pub/png/libpng-manual.txt

        if ( bit_depth == 16 )
                png_set_strip_16( png );

        if ( color_type == PNG_COLOR_TYPE_PALETTE )
                png_set_palette_to_rgb( png );

        // PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
        if ( color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8 )
                png_set_expand_gray_1_2_4_to_8( png );

        if ( png_get_valid( png, info, PNG_INFO_tRNS ) )
                png_set_tRNS_to_alpha( png );

        // These color_type don't have an alpha channel then fill it with 0xff.
        if ( color_type == PNG_COLOR_TYPE_RGB ||
             color_type == PNG_COLOR_TYPE_GRAY ||
             color_type == PNG_COLOR_TYPE_PALETTE )
                png_set_filler( png, 0xFF, PNG_FILLER_AFTER );

        if ( color_type == PNG_COLOR_TYPE_GRAY ||
             color_type == PNG_COLOR_TYPE_GRAY_ALPHA )
                png_set_gray_to_rgb( png );

        png_read_update_info( png, info );

        row_pointers = (png_bytep *)malloc( sizeof( png_bytep ) * height );
        for ( int y = 0; y < height; y++ ) {
                row_pointers[y] =
                    (png_byte *)malloc( png_get_rowbytes( png, info ) );
        }

        png_read_image( png, row_pointers );

        fclose( fp );

        return 0;
}

void
write_png_file( char *file_name )
{
        FILE *fp = fopen( file_name, "wb" );
        if ( !fp )
                abort();

        png_structp png =
            png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
        if ( !png )
                abort();

        png_infop info = png_create_info_struct( png );
        if ( !info )
                abort();

        if ( setjmp( png_jmpbuf( png ) ) )
                abort();

        png_init_io( png, fp );

        // Output is 8bit depth, RGBA format.
        png_set_IHDR( png, info, width, height, 8, PNG_COLOR_TYPE_RGBA,
                      PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
                      PNG_FILTER_TYPE_DEFAULT );
        png_write_info( png, info );

        // To remove the alpha channel for PNG_COLOR_TYPE_RGB format,
        // Use png_set_filler().
        // png_set_filler(png, 0, PNG_FILLER_AFTER);

        png_write_image( png, row_pointers );
        png_write_end( png, NULL );

        for ( int y = 0; y < height; y++ ) {
                free( row_pointers[y] );
        }
        free( row_pointers );

        fclose( fp );
}

int
rgb2cga( int r, int g, int b )
{
        float lum = 0.2126 * r + 0.7152 * g + 0.722 * b;

        if ( lum > 192 )
                return 3;
        if ( lum > 128 )
                return 2;
        if ( lum > 64 )
                return 1;
        return 0;
}

int
png_getPixel( int x, int y )
{
        if ( x >= width || y >= height ) {
                printf( "ERR PNG PIX READ %d, %d OF %d, %d\n", x, y, width - 1,
                        height - 1 );
                return -1;
        }

        png_bytep row = row_pointers[y];
        png_bytep px = &( row[x * 4] );

        int pixel = rgb2cga( px[0], px[1], px[2] );

        // printf( "%d %d %d => %d\n", px[0], px[1], px[2], pixel );
        // printf( "%d", pixel );

        return pixel;
}
