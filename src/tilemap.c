#include "tilemap.h"

#include <stdio.h>
#include <stdlib.h>

#include "display.h"
#include "tile.h"

struct tilemap tilemap_list[TILEMAP_COUNT];
int tilemap_count = 0;

void
rc_tilemap_test()
{
        printf( "TILEMAP TEST START\n" );

        const int width = SCREEN_WIDTH / TILE_SIZE_PIXELS;
        const int height = SCREEN_HEIGHT / TILE_SIZE_PIXELS;

        struct tilemap *tm = rc_tilemap_create( 0, width, height );

        printf( "\t%dx%d TILEMAP (SEQUENTIAL)\n", width, height );
        for ( int x = 0; x < width; ++x ) {
                for ( int y = 0; y < height; ++y ) {
                        tm->tile[x][y] = x % 16;
                }
        }
        rc_tilemap_draw( 0, 0, 0 );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "\tSCROLL UP LEFT\n" );
        for ( int step = 0; step < 200; ++step ) {
                int x = step + step / 2;
                int y = step;
                rc_tilemap_draw( 0, -x, -y );
                rc_display_render();
        }

        printf( "\tSCROLL DOWN RIGHT\n" );
        for ( int step = 0; step < 200; ++step ) {
                int x = step + step / 2;
                int y = step;
                rc_tilemap_draw( 0, x, y );
                rc_display_render();
        }

        printf( "\t%dx%d TILEMAP (TILED)\n", width, height );
        tm->tiled_hori = true;
        tm->tiled_vert = true;
        rc_tilemap_draw( 0, 0, 0 );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "\tSCROLL UP LEFT (TILED)\n" );
        for ( int step = 0; step < 200; ++step ) {
                int x = step + step / 2;
                int y = step;
                rc_tilemap_draw( 0, -x, -y );
                rc_display_render();
                SDL_Delay( 10 );
        }

        printf( "\tSCROLL DOWN RIGHT (TILED)\n" );
        for ( int step = 0; step < 200; ++step ) {
                int x = step + step / 2;
                int y = step;
                rc_tilemap_draw( 0, x, y );
                rc_display_render();
                SDL_Delay( 10 );
        }

        printf( "TILEMAP TEST DONE\n\n" );
}

struct tilemap *
rc_tilemap_create( const int tilemap_id, const int width, const int height )
{
        if ( tilemap_id > tilemap_count ) {
                return NULL;
        }

        struct tilemap *tm = &tilemap_list[tilemap_id];

        tm->width = width;
        tm->height = height;

        // TODO free this memory
        tm->tile = malloc( width * sizeof( int * ) );
        for ( int x = 0; x < width; ++x ) {
                tm->tile[x] = malloc( height * sizeof( int ) );
        }

        tm->tiled_vert = false;
        tm->tiled_hori = false;

        ++tilemap_count;
        return tm;
}

void
rc_tilemap_drawBoth( const struct tilemap *const tm, const int x, const int y,
                     const int ox, const int oy )
{
        const int tile_x = x * TILE_SIZE_PIXELS;
        const int tile_y = y * TILE_SIZE_PIXELS;

        const int tilemap_width = tm->width * TILE_SIZE_PIXELS;
        const int tilemap_height = tm->height * TILE_SIZE_PIXELS;

        // to the left
        int screen_x = ox + tile_x;
        while ( screen_x >= 0 ) {
                screen_x -= tilemap_width;

                // up
                int screen_y = oy + tile_y;
                while ( screen_y >= 0 ) {
                        screen_y -= tilemap_height;
                        rc_tile_drawAtPos( tm->tile[x][y], screen_x, screen_y );
                }

                // down
                screen_y = oy + tile_y;
                while ( screen_y <= SCREEN_HEIGHT ) {
                        screen_y += tilemap_height;
                        rc_tile_drawAtPos( tm->tile[x][y], screen_x, screen_y );
                }
        }

        // to the right
        screen_x = ox + tile_x;
        while ( screen_x <= SCREEN_WIDTH ) {
                screen_x += tilemap_width;

                // up
                int screen_y = oy + tile_y;
                while ( screen_y >= 0 ) {
                        screen_y -= tilemap_height;
                        rc_tile_drawAtPos( tm->tile[x][y], screen_x, screen_y );
                }

                // down
                screen_y = oy + tile_y;
                while ( screen_y <= SCREEN_HEIGHT ) {
                        screen_y += tilemap_height;
                        rc_tile_drawAtPos( tm->tile[x][y], screen_x, screen_y );
                }
        }
}

void
rc_tilemap_drawVert( const struct tilemap *const tm, const int x, const int y,
                     const int ox, const int oy )
{
        const int tilemap_height = tm->height * TILE_SIZE_PIXELS;
        const int tile_y = y * TILE_SIZE_PIXELS;
        int screen_y = oy + tile_y;

        // up
        while ( screen_y >= 0 ) {
                screen_y -= tilemap_height;
                rc_tile_drawOnGridOffset( tm->tile[x][y], x, 0, ox, screen_y );
        }

        // down
        screen_y = oy + tile_y;
        while ( screen_y <= SCREEN_HEIGHT ) {
                screen_y += tilemap_height;

                rc_tile_drawOnGridOffset( tm->tile[x][y], x, 0, ox, screen_y );
        }
}

void
rc_tilemap_drawHori( const struct tilemap *const tm, const int x, const int y,
                     const int ox, const int oy )
{
        const int tilemap_width = tm->width * TILE_SIZE_PIXELS;
        const int tile_x = x * TILE_SIZE_PIXELS;
        int screen_x = ox + tile_x;

        // to the left
        while ( screen_x >= 0 ) {
                screen_x -= tilemap_width;
                rc_tile_drawOnGridOffset( tm->tile[x][y], 0, y, screen_x, oy );
        }

        // to the right
        screen_x = ox + tile_x;
        while ( screen_x <= SCREEN_WIDTH ) {
                screen_x += tilemap_width;
                rc_tile_drawOnGridOffset( tm->tile[x][y], 0, y, screen_x, oy );
        }
}

void
rc_tilemap_draw( const int id, const int ox, const int oy )
{
        if ( id >= tilemap_count ) {
                printf( "ERROR tilemap id out of bounds (%d >= %d)", id,
                        tilemap_count );
                return;
        }

        struct tilemap *tm = &tilemap_list[id];

        for ( int x = 0; x < tm->width; ++x ) {
                for ( int y = 0; y < tm->height; ++y ) {
                        if ( tm->tiled_vert && tm->tiled_hori ) {
                                rc_tilemap_drawBoth( tm, x, y, ox, oy );
                        }

                        if ( tm->tiled_vert ) {
                                rc_tilemap_drawVert( tm, x, y, ox, oy );
                        }

                        if ( tm->tiled_hori ) {
                                rc_tilemap_drawHori( tm, x, y, ox, oy );
                        }

                        rc_tile_drawOnGridOffset( tm->tile[x][y], x, y, ox,
                                                  oy );
                }
        }
}

void
rc_tilemap_fill( int tilemap_id, int tile_id )
{
        if ( tilemap_id >= tilemap_count ) {
                printf( "ERROR tilemap id out of bounds (%d >= %d)", tilemap_id,
                        tilemap_count );
                return;
        }

        struct tilemap *tm = &tilemap_list[tilemap_id];
        for ( int x = 0; x < tm->width; ++x ) {
                for ( int y = 0; y < tm->height; ++y ) {
                        tm->tile[x][y] = tile_id;
                }
        }
}

void
rc_tilemap_set( int tilemap_id, int x, int y, int tile_id )
{
        struct tilemap *tm = &tilemap_list[tilemap_id];
        tm->tile[x][y] = tile_id;
}
