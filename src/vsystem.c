#include "vsystem.h"

#include <stdio.h>
#include <time.h>

// cmake configuration file
#include "retcon-config.h"

#include "debug.h"
#include "display.h"
#include "memory.h"
#include "multisprite.h"
#include "print.h"
#include "sprite.h"
#include "tile.h"
#include "tilemap.h"
#include "types.h"

void rc_vs_shutdown();
void rc_vs_render();
void rc_vs_start();
void rc_vs_init();
void rc_vs_update();

SDL_Event event;

struct vram_info {
        word start;
        word width;
        word height;
        byte color_bits;
};

void
rc_vs_init()
{
        printf( "SYSTEM INIT\n\n" );
        printf( "rETcON ENGINE v%d.%d", VERSION_MAJOR, VERSION_MINOR );

        vsystem.power = 0;
        vsystem.cpu_speed = (int)( 4.77 * 1024 * 1024 );
        vsystem.mem_size = 0x10000;
        vsystem.store_size = 160 * 1024;

        vsystem.vram_start = 0x0a00;
        vsystem.vram_width = 320;
        vsystem.vram_height = 200;
        vsystem.color_bits = 2;
}

// when a key is pressed (single-fire events)
void
rc_vs_keydown( SDL_KeyboardEvent keyEvent )
{
        switch ( keyEvent.keysym.sym ) {
        case SDLK_d:
                rc_display_dbgDumpPixels();
                return;
        case SDLK_ESCAPE:
                rc_vs_shutdown();
                return;
        case SDLK_F1:
                printHeader( "Debug Commands" );
                printf( "ESC\tquit\n"
                        "F1\tshow help\n" );
                return;
        }
}

// when the system is shut off
void
rc_vs_shutdown()
{
        vsystem.power = 0;
        printf( "SHUT DOWN\n\n" );
}

// bootup sequence
int
rc_vs_startup( int scale )
{
        system( "clear" );
        printf( "SYSTEM STARTING\n\n" );

        struct mem_info mem_info;
        mem_info.size = vsystem.mem_size;
        rc_mem_init( &mem_info );

        /*
                struct vram_info vram_info;
                vram_info.start = vsystem.vram_start;
                vram_info.width = vsystem.vram_width;
                vram_info.height = vsystem.vram_height;
                vram_info.color_bits = vsystem.color_bits;
                */

        if ( rc_display_init( scale ) != 0 ) {
                fprintf( stderr, "Failed to init display. Aborting.\n" );
                return 1;
        }

        rc_tile_loadSheet( "grid.png" );
        rc_sprite_init();

        vsystem.power = 1;
        return 0;
}

// main update (inside engine loop)
void
rc_vs_update()
{
        SDL_PumpEvents();
        if ( SDL_PollEvent( &event ) ) {
                if ( event.type == SDL_KEYDOWN )
                        rc_vs_keydown( event.key );
                // if( event.type == SDL_KEYUP )
                // gameboy_keyup( event.key );
                if ( event.type == SDL_QUIT )
                        rc_vs_shutdown();
        }

        int mem_err = rc_mem_getError();
        if ( mem_err == 0 )
                return;

        if ( mem_err == 1 )
                fprintf( stderr, "SEGFAULT\n\n" );
        else
                fprintf( stderr, "UNKNOWN ERROR\n\n" );
        rc_vs_shutdown();
}

void
test_drawCharRam()
{
        /*
        for ( int addr = ADDR_BG_MAP_0_START; addr < ADDR_BG_MAP_0_END;
              ++addr ) {
                rc_mem_set( addr, 0x00 );
        }

        int cram_width_tiles = 16;
        int vram_width_tiles = 64;
        for ( int y = 0; y < cram_width_tiles; ++y ) {
                for ( int x = 0; x < cram_width_tiles; ++x ) {
                        int cram_tile_index = y * cram_width_tiles + x;
                        int vram_tile_index = y * vram_width_tiles + x;
                        int bg_addr = ADDR_BG_MAP_0_START + vram_tile_index;
                        rc_mem_set( bg_addr, cram_tile_index );
                }
        }
        */
}

void
test_drawCharRamLinear()
{
        /*
        for ( int addr = ADDR_BG_MAP_0_START; addr < ADDR_BG_MAP_0_END;
              ++addr ) {
                rc_mem_set( addr, 0x00 );
        }

        int char_count = 256;
        for ( int char_index = 0; char_index < char_count; ++char_index ) {
                int bg_addr = ADDR_BG_MAP_0_START + char_index;
                rc_mem_set( bg_addr, char_index );
        }
        */
}

void
test_printMem( const int start, const int end, const int row_length )
{
        const int length = end - start;
        for ( int i = 0; i < length; ++i ) {
                if ( i % row_length == 0 ) {
                        printf( "\n" );
                }
                printf( "%02X ", rc_mem_get( start + i ) );
        }
        printf( "\n\n" );
}

void
test_printBg0()
{
        printf( "Background 0 tile offsets:\n" );
        test_printMem( ADDR_BG_MAP_0_START, ADDR_BG_MAP_0_END, 64 );
}

void
test_printCharRam()
{
        printf( "Char RAM pixels: " );
        test_printMem( ADDR_CHAR_RAM_START, ADDR_CHAR_RAM_END, 32 );
}

// entry point
int
main( int argc, char **argv )
{
        rc_vs_init();

        // TODO update video to allow fullscreen with user-settable border color
        // for empty space around
        int scale = 3;
        if ( argc > 1 ) {
                scale = atoi( argv[1] );
        }

        if ( rc_vs_startup( scale ) != 0 ) {
                // failed startup
                return 1;
        }

        // tests
        printf( "TESTING SYSTEMS\n\n" );
        // rc_display_test();
        // rc_tile_test();
        // rc_tilemap_test();
        // rc_sprite_test();
        // rc_multisprite_test();
        printf( "TESTING COMPLETE\n\n" );

        // TODO calculate based on CPU speed? or video speed? hmm
        const float TIME_STEP_MS = 1000.0f / 10.0f;

        float time_last_ms = 0.0f;
        float time_accum = 0.0f;
        while ( vsystem.power ) {
                rc_display_render();

                while ( time_accum < TIME_STEP_MS ) {

                        // since we go insanely fast let's throttle a little bit
                        SDL_Delay( 10 );

                        if ( !vsystem.power )
                                break;

                        float time_current_ms = SDL_GetTicks();
                        time_accum += time_current_ms - time_last_ms;

                        rc_vs_update();

                        time_last_ms = time_current_ms;
                }
                time_accum -= TIME_STEP_MS;
        }

        rc_display_destroy();
        SDL_Quit();

        return 0;
}
