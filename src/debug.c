#include "debug.h"

#include <string.h>

void binary( const unsigned int val, char *str ) {
        for( int i = 256; i > 0; i >>= 1 )
                *str++ = ( val & i ) ? '1' : '0';
        *str = '\0';
}
