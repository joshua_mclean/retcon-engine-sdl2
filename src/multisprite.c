#include "multisprite.h"

#include <stdarg.h>
#include <stdlib.h>

#include "display.h"
#include "sprite.h"
#include "tile.h"

#define X 0
#define Y 1

struct multisprite multisprite_list[MAX_SPRITES];

//
// internal functions
//

void
rc_multisprite_update( struct multisprite *ms )
{
        for ( int sprite_id = 0; sprite_id < ms->sprite_count; ++sprite_id ) {
                struct sprite *sprite =
                    rc_sprite_get( ms->sprite_list[sprite_id] );
                sprite->x = ms->x + ms->sprite_offset_list[sprite_id][X];
                sprite->y = ms->y + ms->sprite_offset_list[sprite_id][Y];
        }
}

//
// external functions
//

void
rc_multisprite_test()
{
        printf( "MULTISPRITE TEST START\n" );

        rc_sprite_create( 0, 0 );
        rc_sprite_create( 1, 1 );
        rc_sprite_create( 2, 2 );
        rc_sprite_create( 3, 3 );
        struct multisprite *ms = rc_multisprite_create( 0, 4, 0, 1, 2, 3 );
        rc_multisprite_makeSquare( ms, 2 );

        printf( "\t2x2 MULTISPRITE DRAW\n" );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "\t2x2 MULTISPRITE MOVE\n" );
        for ( int c = 0; c < 50; ++c ) {
                ms->x += rand() % 5;
                ms->y += rand() % 5;
                printf( "\t\t(%d, %d)\n", ms->x, ms->y );
                rc_display_render();
                SDL_Delay( 100 );
        }

        printf( "\tERASE MULTISPRITE\n" );
        rc_multisprite_erase( 0 );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "MULTISPRITE TEST END\n" );
}

struct multisprite *
rc_multisprite_create( const int multisprite_index, const int sprite_count,
                       ... )
{
        struct multisprite *multisprite = &multisprite_list[multisprite_index];
        multisprite->sprite_count = sprite_count;

        // TODO free this memory
        multisprite->sprite_list = malloc( sprite_count * sizeof( int ) );
        multisprite->sprite_offset_list =
            malloc( sprite_count * sizeof( int * ) );
        for ( int i = 0; i < sprite_count; ++i ) {
                multisprite->sprite_offset_list[i] =
                    malloc( 2 * sizeof( int ) );
        }

        // make a sprite for each tile we're given
        va_list args;
        va_start( args, sprite_count );
        for ( int i = 0; i < sprite_count; ++i ) {
                int sprite_index = va_arg( args, int );
                multisprite->sprite_list[i] = sprite_index;
                multisprite->sprite_offset_list[i][X] = 0;
                multisprite->sprite_offset_list[i][Y] = 0;

                rc_sprite_get( sprite_index )->visible = true;
        }

        va_end( args );

        multisprite->visible = true;

        // return the index to the multisprite
        return multisprite;
}

void
rc_multisprite_erase( const int multisprite_index )
{
        struct multisprite *ms = &multisprite_list[multisprite_index];

        for ( int i = 0; i < ms->sprite_count; ++i ) {
                rc_sprite_get( ms->sprite_list[i] )->visible = false;
        }

        ms->visible = false;
}

void
rc_multisprite_makeSquare( struct multisprite *ms, const int width )
{
        for ( int i = 0; i < ms->sprite_count; ++i ) {
                ms->sprite_offset_list[i][X] = i % width * TILE_SIZE_PIXELS;
                ms->sprite_offset_list[i][Y] = i / width * TILE_SIZE_PIXELS;
        }
}

void
rc_multisprite_updateAll()
{
        for ( int i = 0; i < MAX_SPRITES; ++i ) {
                struct multisprite *ms = &multisprite_list[i];
                if ( !ms->visible ) {
                        continue;
                }

                rc_multisprite_update( ms );
        }
}
