#include "tile.h"

#include "display.h"
#include "loadpng.h"
#include "memory.h"

// TODO better naming
#define TILE_COUNT 256
#define TILE_COUNT_X SCREEN_WIDTH / TILE_SIZE_PIXELS
#define TILE_COUNT_Y SCREEN_HEIGHT / TILE_SIZE_PIXELS

struct tile tile_list[TILE_COUNT];
int loaded_tile_count = 0;

void
rc_tile_test()
{
        printf( "TILE TEST START\n" );

        // TODO test draw at pos, draw on grid etc.

        int loaded = rc_tile_loadedCount();
        printf( "\t%d TILES LOADED\n", loaded );
        printf( "\tDRAW ALL TILES\n" );

        int width = sqrt( loaded_tile_count );

        int x = 0;
        int y = 0;
        for ( int i = 0; i < loaded; ++i ) {
                rc_tile_drawOnGrid( i, x, y );
                ++x;
                if ( x >= width ) {
                        x = 0;
                        ++y;
                }
        }

        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "TILE TEST DONE\n\n" );
}

void
rc_tile_drawAtPos( const int tile_index, const int start_x, const int start_y )
{
        // printf( "Tile #%d (%d, %d)\n", tile_index, start_x, start_y );
        struct tile *tile = &tile_list[tile_index];
        for ( int ox = 0; ox < TILE_SIZE_PIXELS; ++ox ) {
                for ( int oy = 0; oy < TILE_SIZE_PIXELS; ++oy ) {
                        const int x = start_x + ox;
                        const int y = start_y + oy;

                        rc_display_drawPixel( x, y, tile->pixels[ox][oy] );
                }
        }
}

void
rc_tile_drawOnGrid( const int tile_index, const int tile_x, const int tile_y )
{
        rc_tile_drawOnGridOffset( tile_index, tile_x, tile_y, 0, 0 );
}

// tile_x/y = tile position (in grid); ox/oy = offset in pixels
void
rc_tile_drawOnGridOffset( const int tile_index, const int tile_x,
                          const int tile_y, const int ox, const int oy )
{
        const int x = tile_x * TILE_SIZE_PIXELS + ox;
        const int y = tile_y * TILE_SIZE_PIXELS + oy;

        rc_tile_drawAtPos( tile_index, x, y );
}

// returns the ID # for the tile that got loaded, -1 for error
int
rc_tile_load( int png_tile_x, int png_tile_y )
{
        const int start_x = png_tile_x * TILE_SIZE_PIXELS;
        const int start_y = png_tile_y * TILE_SIZE_PIXELS;

        const int tile_index = loaded_tile_count;

        for ( int rel_y = 0; rel_y < TILE_SIZE_PIXELS; ++rel_y ) {
                int png_y = start_y + rel_y;

                // for each byte in the tile
                for ( int rel_x = 0; rel_x < TILE_SIZE_PIXELS; ++rel_x ) {
                        int png_x = start_x + rel_x;

                        tile_list[tile_index].pixels[rel_x][rel_y] =
                            png_getPixel( png_x, png_y );
                }
        }

        ++loaded_tile_count;
        return tile_index;
}

int
rc_tile_loadedCount()
{
        return loaded_tile_count;
}

// returns the ID # of the last tile loaded in the sheet, -1 for error
// TIP: get the number of loaded tiles to know where it starts
int
rc_tile_loadSheet( const char *const file_name )
{
        printf( "LOAD PNG %s\n", file_name );

        int err = png_load( file_name );
        if ( err ) {
                printf( "FAILED ERR PNG #%d\n\n", err );
                return -1;
        }

        int png_width, png_height;
        png_getSize( &png_width, &png_height );

        if ( png_width % TILE_SIZE_PIXELS != 0 ||
             png_height % TILE_SIZE_PIXELS != 0 ) {
                printf( "Error: extra pixels at end of tile sheet (%dx%d "
                        "tiles, %dx%d sheet)\n\n",
                        TILE_SIZE_PIXELS, TILE_SIZE_PIXELS, png_width,
                        png_height );
                return -1;
        }

        // TODO calculate if we'll have too many tiles to fit in our list

        const int tile_pixels = TILE_SIZE_PIXELS * TILE_SIZE_PIXELS;
        int png_tile_count = png_width * png_height / tile_pixels;
        int png_tile_width = png_width / TILE_SIZE_PIXELS;
        printf( "PNG %d TILES %d WIDE\n", png_tile_count, png_tile_width );

        // for each tile
        int tile_index = 0;
        for ( int i = 0; i < png_tile_count; ++i ) {

                // upper-left TILE pos in png
                int png_tile_x = i % png_tile_width;
                int png_tile_y = i / png_tile_width;

                // printf( "tile %02X / %02X: ", tile_index, png_tile_count - 1
                // );
                tile_index = rc_tile_load( png_tile_x, png_tile_y );
                if ( tile_index < 0 ) {
                        printf( "Couldn't retrieve tile" );
                        return -1;
                }
        }

        printf( "SUCCESS\n\t%d TILES LOADED %dx%d PIXELS\n\n", png_tile_count,
                png_width, png_height );

        return tile_index;
}
