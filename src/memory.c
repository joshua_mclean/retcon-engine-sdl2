#include "memory.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// internal datate
struct mem_info mem_info;
byte error = MEM_ERROR_NONE;
byte *memory = NULL;

//
// internal functions
//

const byte rc_mem_check( const word addr );
void rc_mem_throwError( const int error_code );

// check that an address is valid; 1 if valid, 0 if not
// unlike rc_mem_isValid, throws an error on failed access
const byte
rc_mem_check( const word addr )
{
        // if we've already thrown an error, we're already lost in the woods
        if ( error != MEM_ERROR_NONE )
                return 0;

        if ( !rc_mem_isValid( addr ) ) {
                rc_mem_throwError( MEM_ERROR_SEGFAULT );
                fprintf( stderr, "Tried to access addr 0x%X, max addr 0x%X\n\n",
                         addr, ( mem_info.size - 1 ) );
                return 0;
        }

        return 1;
}

// TODO maybe a generalized error throwing mechanism would be a good idea?
void
rc_mem_throwError( const int error_code )
{

        // only keep & backtrace first error thrown
        if ( error != MEM_ERROR_NONE )
                return;

        error = error_code;
        show_backtrace();
}

//
// external functions
//

const int
rc_mem_getSize()
{
        return mem_info.size;
}

const byte
rc_mem_isValid( const word addr )
{
        return addr < mem_info.size;
}

const byte
rc_mem_get( const word addr )
{
        if ( !rc_mem_check( addr ) )
                return -1;
        return memory[addr];
}

// get a pointer to a place in memory; check that the size of the range is valid
const byte *const
rc_mem_getRange( const word addr, const word range )
{
        if ( !rc_mem_check( addr ) )
                return NULL;
        if ( !rc_mem_check( addr + range ) )
                return NULL;

        return &memory[addr];
}

const byte
rc_mem_getError()
{
        return error;
}

/*
 * initialize memory for the system to use
 *
 * we like randomness so everything starts random
 *
 * we might want a few sensible defaults in the future
*/
void
rc_mem_init( const struct mem_info *info )
{
        printf( "INIT RAM..." );

        mem_info.size = info->size;
        memory = malloc( sizeof( word ) * mem_info.size );

        printf( "success\n"
                "\t%d BYTES (%dK)\n"
                "\tRANGE 0x%X:0x%X\n"
                "\n",
                mem_info.size, mem_info.size / 1024, 0, mem_info.size - 1 );

        srand( time( NULL ) );
        for ( int i = 0; i < mem_info.size; ++i ) {
                // TODO make a debug switch to do this automatically
                memory[i] = 0; // for debugging
                // memory[i] = rand() % 0xFF;
                // memory[i] = 0x99; // alternating cyan/magenta
        }
}

// set a value in memory
void
rc_mem_set( const word addr, const byte val )
{
        if ( !rc_mem_check( addr ) )
                return;

        memory[addr] = val;
}
