#include <stdio.h>

#include "sprite.h"

#include "display.h"
#include "tile.h"

struct sprite sprite_list[MAX_SPRITES];

void
rc_sprite_test()
{
        printf( "SPRITE TEST START\n" );

        printf( "\tCREATE ONE SPRITE\n" );
        struct sprite *sprite = rc_sprite_create( 0, 0 );
        rc_sprite_setTilePos( sprite, 10, 5 );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "\tMOVE SPRITE\n" );
        for ( int c = 0; c < 50; ++c ) {
                sprite->x += rand() % 5;
                sprite->y += rand() % 5;
                printf( "\t\t(%d, %d)\n", sprite->x, sprite->y );
                SDL_Delay( 100 );
                rc_display_render();
        }

        printf( "\tERASE SPRITE\n" );
        rc_sprite_erase( 0 );
        rc_display_render();
        SDL_Delay( TEST_WAIT_TIME );

        printf( "SPRITE TEST DONE\n\n" );
}

struct sprite *
rc_sprite_create( const int sprite_id, const int tile_index )
{
        struct sprite *sprite = &sprite_list[sprite_id];
        sprite->tile = tile_index;
        sprite->visible = true;
        return sprite;
}

struct sprite *
rc_sprite_createAt( const int sprite_id, const int tile_index, const int x,
                    const int y )
{
        struct sprite *sprite = rc_sprite_create( sprite_id, tile_index );
        sprite->x = x;
        sprite->y = y;
        return sprite;
}

void
rc_sprite_createAtTile( const int sprite_id, const int tile_index,
                        const int tile_x, const int tile_y )
{
        rc_sprite_createAt( sprite_id, tile_index, tile_x * TILE_SIZE_PIXELS,
                            tile_y * TILE_SIZE_PIXELS );
}

void
rc_sprite_draw( const int sprite_id )
{
        if ( sprite_id >= MAX_SPRITES ) {
                return;
        }

        struct sprite *sprite = &sprite_list[sprite_id];
        if ( !sprite->visible ) {
                return;
        }

        rc_tile_drawAtPos( sprite->tile, sprite->x, sprite->y );
}

void
rc_sprite_erase( const int sprite_id )
{
        sprite_list[sprite_id].visible = false;
}

void
rc_sprite_drawAll()
{
        for ( int i = 0; i < MAX_SPRITES; ++i ) {
                rc_sprite_draw( i );
        }
}

struct sprite *
rc_sprite_get( int sprite_id )
{
        return &sprite_list[sprite_id];
}

int
rc_sprite_getId( const struct sprite *sprite )
{
        for ( int i = 0; i < MAX_SPRITES; ++i ) {
                if ( sprite == &sprite_list[i] ) {
                        return i;
                }
        }

        return -1;
}

void
rc_sprite_init()
{
        printf( "SPRITE INIT %d MAX\n\n", MAX_SPRITES );
        for ( int i = 0; i < MAX_SPRITES; ++i ) {
                sprite_list[i].visible = false;
        }
}

void
rc_sprite_setTilePos( struct sprite *const sprite, const int tile_x,
                      const int tile_y )
{
        sprite->x = tile_x * TILE_SIZE_PIXELS;
        sprite->y = tile_y * TILE_SIZE_PIXELS;
}
