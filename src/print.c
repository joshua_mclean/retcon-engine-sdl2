#include <stdio.h>
#include <string.h>

// TODO should be in some kind of output/print file
void printLine( int len ) {
        for( int i = 0; i < len; ++i )
                putchar( '=' );
        putchar( '\n' );
}


// TODO should be in some kind of output/print file
void printHeader( const char *msg ) {
        int len = strlen( msg ) + 2;
        printLine( len );
        printf( " %s\n", msg );
        printLine( len );
}

