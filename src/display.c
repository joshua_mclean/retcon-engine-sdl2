#include "display.h"

#include <stdio.h>

#include "multisprite.h"
#include "sprite.h"
#include "tile.h"

// not always equal with e.g. game boy which is 160x144 but has 256x256 VRAM
// some systems also have multiple "pages" of VRAM which would be handled herc
#define display_width SCREEN_WIDTH
#define display_height SCREEN_HEIGHT

struct {
        SDL_Window *window;
        SDL_Renderer *renderer;
        int disp_width; // TODO set these up from vsystem
        int disp_height;
        int vram_width;
        int vram_height;
        int bg_tilemap;
        int pixels[display_width][display_height];
} display;

void rc_display_setColor( byte color );

//
// internal functions
//

void
rc_display_setColor( byte color )
{
        int r = 0x00;
        int g = 0x00;
        int b = 0x00;

        if ( color == 1 ) {
                r = 0xFF;
                g = 0x55;
                b = 0xFF;
        }
        else if ( color == 2 ) {
                r = 0x55;
                g = 0xFF;
                b = 0xFF;
        }
        else if ( color == 3 ) {
                r = 0xFF;
                g = 0xFF;
                b = 0xFF;
        }

        SDL_SetRenderDrawColor( display.renderer, r, g, b, 255 );
}

//
// external functions
//

void
rc_display_test()
{
        printf( "DISPLAY TEST START\n" );

        // TODO revisit for custom or larger palettes
        for ( int c = 0; c < 4; ++c ) {
                printf( "\tFILL COLOR %d\n", c );
                for ( int x = 0; x < display_width; ++x ) {
                        for ( int y = 0; y < display_height; ++y ) {
                                rc_display_drawPixel( x, y, c );
                        }
                }
                rc_display_render();
                SDL_Delay( 1000 );
        }

        for ( int c = 0; c < 4; ++c ) {
                printf( "\tFILL RANDOM %d\n", c );
                for ( int x = 0; x < display_width; ++x ) {
                        for ( int y = 0; y < display_height; ++y ) {
                                rc_display_drawPixel( x, y, rand() % 4 );
                        }
                }
                rc_display_render();
                SDL_Delay( 1000 );
        }

        printf( "DISPLAY TEST END\n\n" );
}

void
rc_display_clear()
{
        for ( int x = 0; x < display_width; ++x ) {
                for ( int y = 0; y < display_height; ++y ) {
                        // TODO custom clear color
                        rc_display_drawPixel( x, y, 1 );
                }
        }
}

void
rc_display_dbgDumpPixels()
{
        for ( int y = 0; y < display_height; ++y ) {
                for ( int x = 0; x < display_width; ++x ) {
                        printf( "%d ", display.pixels[x][y] );
                }
                printf( "\n" );
        }

        printf( "\n\n" );
}

void
rc_display_destroy()
{
        SDL_DestroyRenderer( display.renderer );
        SDL_DestroyWindow( display.window );
}

void
rc_display_drawPixel( const int x, const int y, const int color )
{
        // skip offscreen pixels
        if ( x < 0 || x >= SCREEN_WIDTH || y < 0 || y >= SCREEN_HEIGHT ) {
                return;
        }

        display.pixels[x][y] = color;
}

int
rc_display_init( int scale )
{
        printf( "DISPLAY INIT x%d\n\n", scale );
        rc_display_clear();

        if ( SDL_Init( SDL_INIT_VIDEO ) != 0 ) {
                fprintf( stderr, "SDL: Init video error.\n" );
                return 1;
        }

        for ( int y = 0; y < display_height; ++y ) {
                for ( int x = 0; x < display_width; ++x ) {
                        display.pixels[x][y] = 0;
                }
        }

        const char *title = "RetCon Engine by Joshua McLean";
        const int x = SDL_WINDOWPOS_CENTERED;
        const int y = x;
        const int width = SCREEN_WIDTH * scale;
        const int height = SCREEN_HEIGHT * scale;
        const Uint32 window_flags = SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS;
        SDL_Window *window =
            SDL_CreateWindow( title, x, y, width, height, window_flags );
        display.renderer =
            SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );

        SDL_RenderSetScale( display.renderer, scale, scale );

        // "clear" the screen
        rc_display_render();

        return 0;
}

void
rc_display_render()
{
        // TODO should these be here? or somehow only draw when updated/changed?
        // rc_tilemap_draw( 0, 0, 0 );
        rc_multisprite_updateAll();
        rc_sprite_drawAll();

        rc_display_setColor( 0 );
        SDL_RenderClear( display.renderer );

        for ( int x = 0; x < display_width; ++x ) {
                for ( int y = 0; y < display_height; ++y ) {
                        rc_display_setColor( display.pixels[x][y] );
                        SDL_RenderDrawPoint( display.renderer, x, y );
                }
        }

        SDL_RenderPresent( display.renderer );

        rc_display_clear();
}
