# RetCon Engine

(c)2017 Joshua McLean [![Twitter](https://upload.wikimedia.org/wikipedia/en/thumb/9/9f/Twitter_bird_logo_2012.svg/1259px-Twitter_bird_logo_2012.svg.png)] (http://www.twitter.com/MrJoshuaMcLean)

## The Idea

The Retro Console engine is based on concepts of developing with classic game
consoles. Instead of creating a full emulator for these systems, RetCon
provides a set of tools to ease development while sticking to the basic ideas
that made these consoles so effective at games.

These ideas include a limited number of tile-based background layers, limited
palettes, overlay sprite handling which utilizes the same tile pool, and
palette swapping as a method of visual interest.

## Sprites

A sprite is a single tiles which can have any position on the screen.

## Multisprites

A multisprite consists of one or more individual sprites. The multisprite
object itself has a position (x, y) from which the contained sprites' (x, y)
positions are relative.

Creating a multisprite:

1. Create each "tile" sprite in the object with rc_sprite_create( sprite_id, tile_index )
2. Use the returned reference to set the sprites' relative positions
3. Create the multisprite object with rc_multisprite_create( multisprite_index, sprite_count, ... ) with the sprite indices of all the created tiles
4. Set the multisprite position which is the top-left "origin" of all attached sprites

