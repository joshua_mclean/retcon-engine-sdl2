# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/joshua/retcon-engine/src/debug.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/debug.c.o"
  "/home/joshua/retcon-engine/src/display.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/display.c.o"
  "/home/joshua/retcon-engine/src/loadpng.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/loadpng.c.o"
  "/home/joshua/retcon-engine/src/memory.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/memory.c.o"
  "/home/joshua/retcon-engine/src/multisprite.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/multisprite.c.o"
  "/home/joshua/retcon-engine/src/print.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/print.c.o"
  "/home/joshua/retcon-engine/src/sprite.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/sprite.c.o"
  "/home/joshua/retcon-engine/src/tile.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/tile.c.o"
  "/home/joshua/retcon-engine/src/tilemap.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/tilemap.c.o"
  "/home/joshua/retcon-engine/src/vsystem.c" "/home/joshua/retcon-engine/CMakeFiles/retcon.dir/src/vsystem.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "include"
  "/usr/include/SDL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
