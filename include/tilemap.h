#ifndef RC_TILEMAP_H
#define RC_TILEMAP_H

#include "types.h"

#define TILEMAP_COUNT 32

struct tilemap {
        int height;
        int **tile;
        int width;
        bool tiled_vert;
        bool tiled_hori;
};

void rc_tilemap_test();

struct tilemap *rc_tilemap_create( const int tilemap_id, const int width,
                                   const int height );
void rc_tilemap_draw( const int id, const int ox, const int oy );
void rc_tilemap_fill( int tilemap_id, int tile_id );

#endif // RC_TILEMAP_H
