#ifndef RC_MULTISPRITE_H
#define RC_MULTISPRITE_H

#include "types.h"

struct multisprite {
        int x;
        int y;
        int sprite_count;
        int **sprite_offset_list;
        int *sprite_list;
        bool visible;
};

void rc_multisprite_test();

struct multisprite *rc_multisprite_create( const int multisprite_index,
                                           const int sprite_count, ... );
void rc_multisprite_erase( const int multisprite_index );
void rc_multisprite_makeSquare( struct multisprite *ms, const int width );
void rc_multisprite_updateAll();

#endif // RC_MULTISPRITE_H
