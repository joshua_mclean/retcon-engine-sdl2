#ifndef RC_SPRITE_H
#define RC_SPRITE_H

#include "types.h"

#define MAX_SPRITES 16

struct sprite {
        int x; // relative in a multisprite
        int y;
        int layer;
        int tile;
        bool visible;
};

void rc_sprite_test();

struct sprite *rc_sprite_create( const int sprite_id, const int tile_index );
void rc_sprite_drawAll();
void rc_sprite_erase( const int sprite_id );
void rc_sprite_init();
struct sprite *rc_sprite_get( int sprite_id );
int rc_sprite_getId( const struct sprite *sprite );
void rc_sprite_setTilePos( struct sprite *const sprite, const int tile_x,
                           const int tile_y );

#endif // RC_SPRITE_H
