#ifndef RC_DISPLAY_H
#define RC_DISPLAY_H

#include <SDL2/SDL.h>

#include "types.h"

#define TILE_SIZE_PIXELS 8

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 200

void rc_display_test();

void rc_display_clear();
void rc_display_dbgDumpPixels();
void rc_display_destroy();
void rc_display_drawPixel( const int x, const int y, const int color );
int rc_display_init(); // TODO pass in vram/disp width/height
void rc_display_render();

#endif // RC_DISPLAY_H
