#ifndef GBP_MEMORY_H
#define GBP_MEMORY_H

#include "types.h"

#define MEM_ERROR_NONE 0
#define MEM_ERROR_SEGFAULT 1

// TODO obvs these have to be variable based on various system definition things
#define ADDR_ROM_BANK_0 0x0000
#define ADDR_ROM_BANK_N 0x4000
#define ADDR_CHAR_RAM_START 0x8000
#define ADDR_CHAR_RAM_END 0x9000
#define ADDR_BG_MAP_0_START 0x9000
#define ADDR_BG_MAP_0_END 0xA000
#define ADDR_BG_MAP_1_START 0xA000
#define ADDR_BG_MAP_1_END 0xB000
#define ADDR_WIN_MAP 0xB000
#define ADDR_SPRITES 0xB3E8
#define ADDR_WORK_RAM 0xBE00
#define ADDR_STACK_BOTTOM 0xFE00
#define ADDR_STACK_TOP 0xFEFF
#define ADDR_PALETTE_DEF 0xFF00
#define ADDR_PALETTE_INFO 0xFF30
#define ADDR_COLOR_INDEX 0xFF31

// sizes in byte
#define COLORS_PER_PALETTE 4
#define MEM_SIZE_COLOR 8
#define MEM_SIZE_MAP 4096
#define MEM_SIZE_PALETTE SIZE_COLOR * 3 * COLORS_PER_PALETTE
#define MEM_SIZE_SPRITE 4
#define MEM_SIZE_TILE 16

struct mem_info {
        int size;
};

const int rc_mem_getSize();

const byte rc_mem_isValid( const word addr );
void rc_mem_init( const struct mem_info *info );
const byte rc_mem_get( const word addr );
const byte *const rc_mem_getRange( const word addr, const word range );
const byte rc_mem_getError();
void rc_mem_set( const word addr, const byte val );

#endif // GBP_MEMORY_H
