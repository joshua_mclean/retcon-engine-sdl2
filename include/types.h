#ifndef GBP_TYPES_H
#define GBP_TYPES_H

#include <execinfo.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef unsigned char byte;

// TODO redefine as size of memory for system
typedef unsigned short word;

// macros (these aren't necessarily type things...)
#define BIT( X, Y ) ( ( ( X ) >> ( Y ) ) & 1 )

// in ms
#define TEST_WAIT_TIME 3 * 1000

static inline int
pow2( int x )
{
        if ( x == 0 )
                return 1;
        int val = 2;
        for ( int i = 1; i < x; ++i ) {
                val *= 2;
        }
        return val;
}

static inline void
printBits( byte n )
{
        for ( int i = 7; i >= 0; --i ) {
                printf( "%d", BIT( n, i ) );
        }
}

static inline void
printBitsRange( byte n, int dig )
{
        if ( dig <= 0 || dig > 8 )
                return;
        for ( int i = dig - 1; i >= 0; --i ) {
                printf( "%d", BIT( n, i ) );
        }
}

// TODO this is only for linux so do something else for non-linux
#define BUF_SIZE 1024
static inline void
show_backtrace()
{
        void *buffer[BUF_SIZE];
        const int num_ptrs = backtrace( buffer, BUF_SIZE );
        char **strings = backtrace_symbols( buffer, num_ptrs );
        if ( !strings ) {
                fprintf( stderr, "Failed to print backtrace - sorry :'(\n\n" );
                return;
        }

        for ( int i = 0; i < num_ptrs; ++i ) {
                printf( "%s\n", strings[i] );
        }

        free( strings );
}

#endif // GBP_TYPES_H
