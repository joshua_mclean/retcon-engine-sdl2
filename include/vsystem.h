#ifndef RE_VSYSTEM_H
#define RE_VSYSTEM_H

// the configuration and manipulation of the "virtual system"

#include <SDL2/SDL.h>

struct {
    // system
    int power;
    int cpu_speed;

    // memory
    int mem_size;
    int store_size;

    // graphics
    int color_bits;
    int disp_width;
    int disp_height;
    int vram_start;
    int vram_width;
    int vram_height;
} vsystem;

#endif // GBP_GAMEBOY_H
