#ifndef RE_LOADPNG_H
#define RE_LOADPNG_H

#include <png.h>
#include <stdlib.h>

void png_getSize( int *w, int *h );
int png_load(const char * const filename); 
int png_getPixel( int x, int y );

#endif // RE_LOADPNG_H
