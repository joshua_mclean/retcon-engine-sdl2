#ifndef RE_TILE_H
#define RE_TILE_H

// TODO defined from vsystem
#define TILE_SIZE_PIXELS 8

struct tile {
        int pixels[TILE_SIZE_PIXELS][TILE_SIZE_PIXELS];
};

void rc_tile_test();

void rc_tile_drawAtPos( const int tile_index, const int start_x,
                        const int start_y );
void rc_tile_drawOnGrid( const int tile_index, const int tile_x,
                         const int tile_y );
void rc_tile_drawOnGridOffset( const int tile_index, const int tile_x,
                               const int tile_y, const int ox, const int oy );
int rc_tile_loadedCount();
int rc_tile_loadSheet( const char *const file_name );

static inline int
rc_gridPos( int tile )
{
        return tile * TILE_SIZE_PIXELS;
}

#endif // RE_TILE_H
